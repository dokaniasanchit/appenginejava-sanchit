package com.cars24.dealerappv2;

import com.cars24.dealerappv2.AppEngine.VertxProvider;
import com.cars24.dealerappv2.auction.AuctionRoutes;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import java.time.Instant;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        Router router = Router.router(vertx);
        router.get("/").handler(this::handleMainPage);
        AuctionRoutes auctionRoutes  = new AuctionRoutes();//MakeThisCode to a class creating listners to all routes for future
        auctionRoutes.setRoutes(router);
        vertx.createHttpServer().requestHandler(router::accept).listen(config().getInteger("http.port", 8080));    
        System.out.println("HTTP server started on port 8080");
    }
    
    private void handleMainPage(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        response.putHeader("content-type", "text/plain");
        response.end("<h1>Dealer App Version2</h1>");
    }
    
    public static void main(String[] args) {
        Vertx vertx = VertxProvider.getVertx();
        DeploymentOptions options = new DeploymentOptions()
          .setConfig(new JsonObject()
                .put("http.port", 8080)
          );
        vertx.deployVerticle(MainVerticle.class.getName(), options);
        System.out.println("MainVerticleDeployed");
        vertx.setPeriodic(500, new Handler<Long>(){ //This is the ticker call to check auction is over or not
          @Override
          public void handle(Long aLong) {
              vertx.eventBus().publish("Ticker", "Ticker is awsome");
          }
       });
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cars24.dealerappv2.AppEngine;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author user
 */
public class BaseController {
    
    protected HttpServerRequest httpRequest;
    protected HttpServerResponse httpResponse;
    private Map<String,Object> requestBody;
    private Map<String,String> urlParams;
    public BaseController(RoutingContext routingContext) {
        this.httpRequest = routingContext.request();
        this.httpResponse = routingContext.response();
        this.generateRequestBody(routingContext);
        this.generateRequestParams(routingContext);
        this.httpResponse.putHeader("content-type", "application/json; charset=utf-8");
    }
    
    private void generateRequestBody(RoutingContext routingContext){
        ObjectMapper mapper = new ObjectMapper();
        try {
            this.requestBody = mapper.readValue(routingContext.getBodyAsString(), HashMap.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void generateRequestParams(RoutingContext routingContext) {
       String[] queryParams = routingContext.request().query().split("&");
       this.urlParams = new HashMap<String,String>();
            try{
                for (String queryParam : queryParams) {
                    int index = queryParam.indexOf('=');
                    String key = queryParam.substring(0, index);
                    key = URLDecoder.decode(key, "UTF-8");

                    String value = null;

                    if (index > 0) {
                     value = queryParam.substring(index + 1);
                     value = URLDecoder.decode(value, "UTF-8");
                    }
                    this.urlParams.put(key, value);
                }
             } catch (Exception e){
                    e.printStackTrace();
            }
    }
    
    public Map<String,Object> getRequestBody(){
        return this.requestBody;
    }
    
    public Map<String,String> getUrlParams(){
        return this.urlParams;
    }
    
    
}

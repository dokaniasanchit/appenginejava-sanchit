/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cars24.dealerappv2.AppEngine;

import io.vertx.core.Vertx;

/**
 *
 * @author user
 */
public class VertxProvider {
    
    private static Vertx vertx;
    public static Vertx getVertx(){
        if(vertx == null)
            vertx = Vertx.vertx();
        return vertx;
    }
    
}

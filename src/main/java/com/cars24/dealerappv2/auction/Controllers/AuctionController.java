package com.cars24.dealerappv2.auction.Controllers;

import com.cars24.dealerappv2.AppEngine.BaseController;
import com.cars24.dealerappv2.AppEngine.VertxProvider;
import com.cars24.dealerappv2.auction.Verticles.NormalAuctionVerticle;
import io.vertx.ext.web.RoutingContext;
import java.time.Instant;

/**
 *
 * @author Dushant Singh <dushant.singh@cars24.com>
 */
public class AuctionController extends BaseController{

    public AuctionController(RoutingContext routingContext) {
        super(routingContext);
    }
    
    
    public void startAuction(){
            int appointmentId = (int) this.getRequestBody().get("appointmentId");
            if(!VertxProvider.getVertx().sharedData().getLocalMap("RunningAuctions").containsKey(appointmentId)){
                VertxProvider.getVertx().deployVerticle(new NormalAuctionVerticle(Instant.now(),12345,4321,appointmentId));
                this.httpResponse.end("Auction Started");
            }
            else
                this.httpResponse.end("Auction Already Running");
            
    }
}

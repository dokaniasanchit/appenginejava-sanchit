
package com.cars24.dealerappv2.auction.Verticles;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class NormalAuctionVerticle extends BaseAuctionVerticle {
    
    public NormalAuctionVerticle( Instant auctionEndtime, int stepRate, int highestBid,int appointmentId) {
        this.setAuctionEndTime(auctionEndtime.plus(1,ChronoUnit.MINUTES));
        this.setAuctionStartTime(Instant.now());
        this.setStepRate(stepRate);
        this.setHighestBidAmount(highestBid);
        this.setAppointmentId(appointmentId);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.out.println("Verticle stop Called");    
    }

    
    @Override
    public void start() throws Exception {
        super.start();
        System.out.println("Verticle start Called");
        vertx.eventBus().consumer("Ticker", new Handler<Message<String>>() {
            @Override
            public void handle(Message<String> message) {
                System.out.println("listing to ticker");
                if (Instant.now().isAfter(getAuctionEndTime())){
                    System.out.println("Auction Time Over");
                    try {
                        vertx.undeploy(deploymentID());
                    } catch (Exception ex) {
                        Logger.getLogger(NormalAuctionVerticle.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }
    
     
    
}

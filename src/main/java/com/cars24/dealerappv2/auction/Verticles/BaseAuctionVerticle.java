package com.cars24.dealerappv2.auction.Verticles;

import io.vertx.core.AbstractVerticle;
import java.time.Instant;

/**
 * Base  Auction verticle to handle a running auction
 * @author Dushant Singh <dushant.singh@cars24.com>
 */
public class BaseAuctionVerticle extends AbstractVerticle {

    public BaseAuctionVerticle() {
        //BaseAuctionVerticle Constructor
    }
    
    private Instant auctionStartTime;
    private Instant auctionEndTime;
    private int highestBidAmount;
    private int stepRate; 
    private int appointmentId;

    public int getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(int appointmentId) {
        this.appointmentId = appointmentId;
    }


    public int getHighestBidAmount() {
        return highestBidAmount;
    }

    public int getStepRate() {
        return stepRate;
    }

    public void setHighestBidAmount(int highestBidAmount) {
        this.highestBidAmount = highestBidAmount;
    }

    public void setStepRate(int stepRate) {
        this.stepRate = stepRate;
    }

    public void setAuctionStartTime(Instant auctionStartTime){
        this.auctionStartTime = auctionStartTime;
    }

    public void setAuctionEndTime(Instant auctionEndTime) {
        this.auctionEndTime = auctionEndTime;
    }

    public Instant getAuctionStartTime() {
        return auctionStartTime;
    }

    public Instant getAuctionEndTime() {
        return auctionEndTime;
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        vertx.sharedData().getLocalMap("RunningAuctions").removeIfPresent(this.getAppointmentId(),true);
    }

    @Override
    public void start() throws Exception {
        super.start();
        vertx.sharedData().getLocalMap("RunningAuctions").putIfAbsent(this.getAppointmentId(),true);
    }
    
    
}

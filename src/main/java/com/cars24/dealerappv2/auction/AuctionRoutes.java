package com.cars24.dealerappv2.auction;

import com.cars24.dealerappv2.auction.Controllers.AuctionController;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

/**
 * AuctionRoutes Class 
 * @author Dushant Singh <dushant.singh@cars24.com>
 */
public class AuctionRoutes {

    public AuctionRoutes() {
        System.out.println("RouterMain");
    }
    
    public void setRoutes(Router router){
        router.route().handler(BodyHandler.create());
        router.get("/auction/:appointmentId").handler(this::getAuctionStatus);
        router.post("/startauction").handler(this::startAuction);
        router.post("/placeBid").handler(this::placeBid);
    }
    
    /**
     * 
     * @param routingContext 
     */
    private void startAuction(RoutingContext routingContext){
        AuctionController auctionController = new AuctionController(routingContext);
        auctionController.startAuction();
    }
    
    /**
     * 
     * @param routingContext 
     */
    private void getAuctionStatus(RoutingContext routingContext){
       
    }
    
    /**
     * 
     * @param routingContext 
     */
    private void placeBid(RoutingContext routingContext){
    
    }
}
